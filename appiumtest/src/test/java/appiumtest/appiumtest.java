package appiumtest;


import java.net.URL;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class appiumtest {

	static AppiumDriver<MobileElement> driver;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
				
		try {
			OpenNeemacademy();
		}
		catch (Exception e) {
			System.out.print(e.getCause());
			System.out.print(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void OpenNeemacademy() throws Exception {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("deviceName", "LAVA_A3");
		cap.setCapability("udid", "L2I640GA2E880C6A");
		cap.setCapability("platformName", "Android");
		cap.setCapability("platformVersion", "6.0");
		
		cap.setCapability("appPackage", "com.neemacademy.learner");
		cap.setCapability("appActivity", "md57974a8cc6d5f274be6df6f05a5fba3f8.MainActivity");
		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AppiumDriver<MobileElement>(url,cap);
		
		MobileElement allow = driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button"));
		allow.click();
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("direction", "down");
		js.executeScript("mobile: swipe", scrollObject);
		
		
		System.out.print("Application Started......");
	}

}
