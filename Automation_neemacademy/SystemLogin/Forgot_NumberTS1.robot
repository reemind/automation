*** Settings ***
Library    SeleniumLibrary
Resource    ../Resourse/Resourse.robot
Resource    ../Resourse/Resource1.robot
Test Setup    Start Browser and Maximize 


       
*** Test Cases ***
Wrong Number
    ForgotPassword Link Click
    Input Forgot Number    7929898
    PasswordReset link Send
    Wrong Forgot Mail Message Should be    Enter valid email or mobile number
    
Not Verified or Not Registered Number
    ForgotPassword Link Click
    Input Forgot Number    9874563210
    PasswordReset link Send
    Not Verified Forgot Mail Message Should Be    User not found!
    
Registered and Verified Number
    ForgotPassword Link Click
    Input Forgot Number    9874563210
    PasswordReset link Send
    Registered Number Message Should Be    Password reset code is sent to your phone
    
Empty ForgotEmail
    ForgotPassword Link Click
    Input Forgot Number    ${Empty}
    PasswordReset link Send
    Empty Forgot Mail Message Should Be    Email or Phone is required.
    
    
    
    