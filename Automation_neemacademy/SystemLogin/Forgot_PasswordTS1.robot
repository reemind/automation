*** Settings ***
Library    SeleniumLibrary
Resource    ../Resourse/Resourse.robot
Resource    ../Resourse/Resource1.robot
Test Setup    Start Browser and Maximize 


       
*** Test Cases ***

Verified ForgotEmail
    ForgotPassword Link Click
    Input ForgotEmail    mahesh@yopmail.com
    PasswordReset link Send
    Sleep    10    
    Element Text Should Be    id=resetSuccessMsg    Please check your email for password reset link    
 
Invalid ForgotEmail
    ForgotPassword Link Click
    Input ForgotEmail    mahesh1123@yopmail.com
    PasswordReset link Send
    Invalid Forgot Password Message Should Be    User not found!
    
NotVerified ForgotEmail
    ForgotPassword Link Click
    Input ForgotEmail    1232mahesh@yopmail.com
    PasswordReset link Send
    Not Verified Forgot Mail Message Should Be    User not found!
Empty ForgotEmail
    ForgotPassword Link Click
    Input ForgotEmail    ${Empty}
    PasswordReset link Send
    Empty Forgot Mail Message Should Be    Email or Phone is required.
    
Wrong ForgotEmail
    ForgotPassword Link Click
    Input ForgotEmail    679j
    PasswordReset link Send
    Wrong Forgot Mail Message Should be    Enter valid email or mobile number

    
  
    