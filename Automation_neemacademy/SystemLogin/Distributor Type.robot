*** Settings ***
Library    SeleniumLibrary
Library    Collections    
Library    RequestsLibrary
Library    String 
Library    JSONLibrary    
Library    SeleniumLibrary    
       

Resource    ../Resourse/Resourse.robot
Resource    ../Resourse/Resource1.robot
# Test Setup   Start Browser and Maximize
Resource    ../Resourse/RequestHandler.robot
*** Variables ***
${baseurl}    http://stagingapi.neemacademy.com
${Auth}    Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBUElVc2VyIiwianRpIjoiZjAyZDljY2MtMTQ2NS00OWVkLTk3YmMtM2YzMDQ4ZjE3NDI5IiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIkFQSVVzZXIiLCJTdXBlclVzZXIiXSwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6IkFQSVVzZXIiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3VzZXJkYXRhIjoie1wiVXNlcklEXCI6MSxcIlVzZXJOYW1lXCI6XCJ1c2VyX2lkXzFcIixcIklzVmFsaWRUb2tlblwiOmZhbHNlLFwiUm9sZXNcIjpbXX0iLCJuYmYiOjE1NzM0NjI3NTUsImV4cCI6MTU3MzQ2OTk1NSwiaXNzIjoiTmVlbWFjYWRlbXkuQVBJVG9rZW5Jc3N1ZXIiLCJhdWQiOiJodHRwczovL3N0YWdpbmdhcGkubmVlbWFjYWRlbXkuY29tIn0.Fphg6Cs1dIo1MlWgAzmp9ksbM3IS1AzvCePfgBDKuvI
${type}    application/json
${distributor_type}    Distributor_type

*** Keywords ***



*** Test Cases ***

AddDistributor_Type
    
    ${body} =     Create Dictionary    DistributorTypeName=rajkumarkhanal   Percentage=10 
    ${res}=  PostRequestCommonFunction    /api/Distributors/CreateDistributorType    ${body}
    Log To Console    ${res.status_code}
    Log To Console    ${res.content}    
   
APIDeleteDistributor_Type
    # ${body} =     Create Dictionary    DistributorTypeName=rajkumarkhanal   Percentage=10 
    ${res}=  PostRequestCommonFunction    /api/Distributors/DeleteDistributorType?distTypeId=23    ${empty}
    Log To Console    ${res.status_code}
    Log To Console    ${res.content}    
    

GetDistributorList_Type
    
# GetRequestCommonFunction need  ${sub_url}   
    ${res}=    GetRequestCommonFunction    /api/Distributors/GetDistributorTypeListing
    Log To Console    ${res.status_code}
    Log To Console    ${res.json()}
    
GetDistributor_TypeByID
     ${res}=    GetRequestCommonFunction    /api/Distributors/GetDistributorTypeByID?disTypeId=24
     Log To Console    ${res.status_code}
    Log To Console    ${res.json()}
# Just For Understanding 
    # Create Session    Stagingapi    ${baseurl}
    # ${header} =    Create Dictionary     Content-Type=${type}    Authorization=${Auth}
    # ${Response} =  Get Request    Stagingapi    /api/Distributors/GetDistributorTypeListing    headers=${header}   
    # Log To Console    ${Response.status_code}
    # Log To Console    ${Response.json()}
    # Should Contain    ${Response.json()}    Success     


    




    
Submit Distributor with Empty Fields
    Add Distributor Type page Open
    Input Distributor Type    ${empty} 
    Input Percentage    ${empty}
    Click Save Button
    Element Text Should Be    id=disPercentage-error    Percentage is required 
    Element Text Should Be    disTypeName-error     Distributor type name is required       
          
    
      
Submit with Only DIstributor Type Name
    Add Distributor Type page Open
    Input Distributor Type    Haru
    Input Percentage    ${empty}
    Click Save Button
    Distributor's Name without Percentage Message Should Be    Percentage is required
    
Submit With Only Percentage
   Add Distributor Type page Open
   Input Distributor Type    ${empty}
   Input Percentage    2
   Click Save Button
   Add distributor's without Name Message Should Be    Distributor type name is required
    
Submit With Both Name And Percentage
    Add Distributor Type page Open
    Input Distributor Type    Tommo
    Input Percentage    2
    Click Save Button
    
Edit Distributor Type
    Login
    Click Element    xpath=(//span)[21]
    Click Link       id=distributorMenu
    Click Element    //i[@class="fas fa-pencil-alt"]  
    
Delete Distributor Type
    Login
    Click Element    xpath=(//span)[21]
    Click Link       id=distributorMenu 
    Click Element    xpath=(//i[@class="fas fa-trash-alt"])[5]    
    Click Button     id=okey 
    Element Text Should Be    id=messageSpan    Distributor Type deleted successfully.
    

        
    
    
    

    
    

    

    

   
   
    
    
    




