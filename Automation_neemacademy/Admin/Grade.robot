*** Settings ***
Library    SeleniumLibrary
Library    Collections    
Library    RequestsLibrary
Library    String 
Library    JSONLibrary    
Library    SeleniumLibrary    
       

Resource    ../Resourse/Resourse.robot
Resource    ../Resourse/Resource1.robot
# Test Setup   Start Browser and Maximize
Resource    ../Resourse/RequestHandler.robot

*** Variables ***

${jsonobj}    empty
 
*** Test Cases ***
AddNew_Grade
    ${body} =    Create Dictionary    gradeName=gr    gradeNameNepali=gr
    ${res} =    PostRequestCommonFunction    /api/Grades/AddNewGrade    ${body}
    Log To Console    ${res.status_code}
    Log To Console    ${res.json()}
    # ${data}=    Evaluate    ${res.json()}
    # Log To Console    ${data['Success']}    
    #Run Keyword If    ${res.Success}=False    Log To Console    messages is here        
     # ${data}=    Convert To String    ${res.json()}
     # Should Contain    ${data}    False 
     
Edit_Grade
    ${body} =    Create Dictionary    gradeName=gr7    gradeNameNepali=gr
    ${res} =    PutRequestCommonFunction    /api/Grades/UpdateGrade    ${body}
    Log To Console    ${res.status_code}
    Log To Console    ${res.json()}
    
Delete_Grade
    ${res} =    DeleteRequestCommonFUnction    api/Grades/85    
    Log To Console    ${res.status_code}
    Log To Console    ${res.json()}
    
Add_Subject
    ${body} =    Create Dictionary  
    ${res} =    PostRequestCommonFunction    api/Subjects/InsertSubject    ${body}  
    Log To Console    ${res.status_code}   
    Log To Console    ${res.json()} 
        
Delete_Subject
    ${res} =    DeleteRequestCommonFUnction    api/Subjects/80    
    Log To Console    ${res.status_code}
    Log To Console    ${res.content}     
     
  