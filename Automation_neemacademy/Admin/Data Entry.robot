*** Settings ***
Library    SeleniumLibrary
Resource    ../Resourse/Resourse.robot
Resource    ../Resourse/Admin.robot
Test Setup   Start Browser and Maximize

*** Test Cases ***
Add new Grade
    Login
    Click Element    id=navbarDataEntryDropdown
    Click Element    id=gradeMenu    
    Click Element    //button[@class="btn btn-outline-success mr-2"]   
    Input Text    id=gradeName    CSss    
    Input Text    id=gradeNameNepali    CSss
    Click Button    xpath=/html/body/div[3]/div[2]/div/div[8]/div/div/div/div[2]/div/button[1]
Edit Grade Name
    Login
    Click Element    id=navbarDataEntryDropdown    
    Click Element    id=gradeMenu    
    Click Element    xpath=(//i[@class="fas fa-pencil-alt"])
    Input Text    id=gradeName    CSS    
    Click Button    xpath=/html/body/div[3]/div[2]/div/div[8]/div/div/div/div[2]/div/button[1]    
    
Delete Grade
    Login
    Click Element    id=navbarDataEntryDropdown    
    Click Element    id=gradeMenu 
    Click Element    xpath=(//i[@class="fas fa-trash-alt"])  
      
    Switch Window
    Log To Console    Iam here    
    Click Element    xpath=/html/body/div[3]/div[2]/div/div[7]/div/div/div/div[3]/button[1]  
    
Add new Subject
    Login
    Click Element    id=navbarDataEntryDropdown    
    Click Element    id=subjectMenu  
    Click Element    //button[@class="btn btn-outline-success mr-2"]
    Select Grade    2
    Input Text    id=subjectName    wert
    Input Text    id=subjectNameNepali    wert    
    Click Element    xpath=/html/body/div[3]/div[2]/div/div[8]/div/div/div/div[2]/div/button[1] 
    
Edit Subject Name
    Login
    Click Element    id=navbarDataEntryDropdown    
    Click Element    id=subjectMenu    
    Click Element    xpath=(//i[@class="fas fa-pencil-alt"])
    Input Text    id=subjectName    wertt               
    Click Element    xpath=/html/body/div[3]/div[2]/div/div[8]/div/div/div/div[2]/div/button[1]
    
Delete Subject
    Login
    Click Element    id=navbarDataEntryDropdown    
    Click Element    id=subjectMenu 
    Click Element    //i[@class="fas fa-trash-alt"]    
    Click Element    xpath=/html/body/div[3]/div[2]/div/div[7]/div/div/div/div[3]/button[1] 
    
Add new Chapter Area
    Login
    Click Element    id=navbarDataEntryDropdown 
    Click Element    id=chapterAreaMenu 
    Click Element    //button[@class="btn btn-outline-success mr-2"]    
    Select Grade    2
    Select Subject    2
    Input Text    id=chapterName    chapterName
    Input Text    id=chapterNameNepali    chapterNameNepali
    Click Element    xpath=/html/body/div[3]/div[2]/div/div[8]/div/div/div/div[2]/div/button[1]
    
Edit Chapter Area Name
    Login
    Click Element    id=navbarDataEntryDropdown 
    Click Element    id=chapterAreaMenu 
    Click Element    xpath=(//i[@class="fas fa-pencil-alt"])          
    Input Text    id=chapterName    chapterName
    Click Element    xpath=/html/body/div[3]/div[2]/div/div[8]/div/div/div/div[2]/div/button[1]
    
Delete Chapter Area
    Login
    Click Element    id=navbarDataEntryDropdown 
    Click Element    id=chapterAreaMenu  
    Click Element    //i[@class="fas fa-trash-alt"]     
    Click Element    xpath=/html/body/div[3]/div[2]/div/div[7]/div/div/div/div[3]/button[1]    
    
Add new Chapter
    Login
    Click Element    id=navbarDataEntryDropdown    
    Click Element    id=chapterMenu
    Click Element    //button[@class="btn btn-outline-success mr-2"]      
    Select Grade    2
    Select Subject    5
    Select Chapter Area    1
    Input Text    id=orderNumber    5    
    Input Text    id=chapterName    chapterName    
    Input Text    id=chapterNameNepali    chapterNameNepali    
    Click Element    xpath=/html/body/div[3]/div[2]/div/div[8]/div/div/div/div[2]/div/button[1]    
    
Edit Chapter Name
    Login
    Click Element    id=navbarDataEntryDropdown    
    Click Element    id=chapterMenu
    Click Element    xpath=(//i[@class="fas fa-pencil-alt"])
    Input Text    id=chapterName    chapterName
    Click Element    xpath=/html/body/div[3]/div[2]/div/div[8]/div/div/div/div[2]/div/button[1] 
    
Delete Chapter
    Login
    Click Element    id=navbarDataEntryDropdown    
    Click Element    id=chapterMenu
    Click Element    //i[@class="fas fa-trash-alt"] 
    Click Element    xpath=/html/body/div[3]/div[2]/div/div[8]/div/div/div/div[2]/div/button[1]


         