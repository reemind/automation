*** Settings ***
Library    SeleniumLibrary
Resource    ../Resourse/Resourse.robot
Resource    ../Resourse/Admin.robot
Test Setup   Start Browser and Maximize

#Resource    ../Resourse/RequestHandler.robot

*** Test Cases ***

#GetDistributorCommissionRule
   #${res}=  GetRequestCommonFunction    /api/CommissionRule/GetDistributorCommissionRuleWithPagination?currentPage=1&pageSize=10&searchTerm=
   #Log To Console    ${res.status_code}    
   #Log To Console    ${res.json()}     
Add New Commission Rule
    Login
    Click on Distributor Menu
    Click on Commission Rule
    Click Add New Button
    Input Commission Rule    Mouses
    Click to Select Distributor Type
    Select Distributor Type    5
    Click to Select Course Category
    Select Course Category    2
    Input Percentage for Commission Rule    3
    Click Save button 
    
Delete Commission Rule
    Login
    Click on Distributor Menu
    Click on Commission Rule
    Click Delete Button    2
    Click Okay Button
    Deleted Commission Message Should be    Distributor Commission Rule deleted successfully.
    
Edit Commission Rule Name
    Login
    Click on Distributor Menu
    Click on Commission Rule
    Click edit button    3
    Input Commission Rule    Rat
    Click Save button 
    
Click Save Button Empty
    Login
    Click on Distributor Menu
    Click on Commission Rule
    Click Save button
    Commission Rule Error Message Should Be    Commission Rule is required.
    Commission Rule Percentage Error Message Should Ne    Percentage is required
    

    

    
    

    
    
    
       
    
    

    
    
    

    
      
    
    
