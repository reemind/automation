*** Settings ***
Library    SeleniumLibrary
Resource    ../Resourse/Resourse.robot
Resource    ../Resourse/Admin.robot
Test Setup   Start Browser and Maximize

*** Test Cases ***
Add new Bonus Tier
    Login
    Click on Distributor Menu
    Click On Bonus Tiers
    Click Add New Button
    Input Bonus Tier Name    Sky
    Input Bonus Range From    2
    Input Bonus Range To    4
    Input Bonus Tier Percentage    3
    Click Save button
    
Delete Bonus Tier
    Login
    Click on Distributor Menu
    Click On Bonus Tiers
    Bonus Tier Delete    12
    Click Okay Button
    Deletion Success Message Should Be    Bonus Tier deleted successfully.
    
Edit Bonus Tier Name
    Login
    Click on Distributor Menu
    Click On Bonus Tiers
    Edit Bonus Tier Name    1
    Input Bonus Tier Name    Dog
    Click Save button
    
Click Save Button Empty
    Login
    Click on Distributor Menu
    Click On Bonus Tiers
    Bonus Tier Name Error Message Should Be    Bonus tier name is required
    Bonus Range From Error Message Should Be    Bonus Range From is required
    Bonus Range To Error Message Should Be    Bonus Range To is required
    Bonus Percentage Error Message Should Be    Bonus percentage is required
    
    

    
    
