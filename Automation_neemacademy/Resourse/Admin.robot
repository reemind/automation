*** Settings ***
Library    SeleniumLibrary  
Library    ../ExternalKeywords/UserKey.py    

*** Variables ***
${URL}    https://staging.neemacademy.com
${Browser}    chrome
${valid username}    mahesh@yopmail.com
${valid password}    1234a
${delay}    1

*** Keywords ***
Input Email
    [Arguments]    ${username}
    Input Text    id = UserEmail    ${username}

    
Input Password
    [Arguments]    ${password}
    SeleniumLibrary.Input Password    id = UserPassword    ${password}

  
Submit 
    Click Button    id = Login
    
Login
    Input Email    admin@neemacademy.com
    Input Password    User@123
    Submit
    
Click on Distributor Menu
    Click Element   id=distributorDropDown
    
Click on Commission Rule
    Click Link       id=commRuleMenu 
    
Click Add New Button 
    Click Element    //button[@class="btn btn-outline-success mr-2"]
    
Input Commission Rule
    [Arguments]    ${rule}
    Input Text    id=CommissionRuleName    ${rule}   
    
Click to Select Distributor Type
    Click Element    id=DistributorType     
    
Select Distributor Type
    [Arguments]    ${num}
    Select From List By Value    id=DistributorType    ${num}  
    
Click to Select Course Category
    Click Element    id=PackageType    
    
Select Course Category
    [Arguments]    ${num}
    Select From List By Value    id=PackageType    ${num}
    
Input Percentage for Commission Rule
    [Arguments]    ${per}
    Input Text    id=percentage    ${per} 
           
Click Save button 
    Click Element    //button[@class="btn btn-primary"]  
    
Click Delete Button
    [Arguments]    ${num}
    Click Element    //i[@class="fas fa-trash-alt"]    ${num}     
                    
Click Okay Button
    Click Element    id=okey
  
Deleted Commission Message Should be
    [Arguments]    ${message}
    Element Text Should Be    id=messageSpan    ${message}
    
Click edit button
    [Arguments]    ${num}
    Click Element    xpath=(//button[@title="Edit Commission Rule"])    ${num}    
    
Click On Bonus Tiers
    Click Element    //a[@href="/Distributor/BonusTier/Index"]
    
#Click Add new Bonus Button
    #Click Element     //button[@class="btn btn-outline-success mr-2"]   
    
Input Bonus Tier Name
    [Arguments]    ${name}
    Input Text    id=bonusTierName    ${name}    
    
Input Bonus Range From
    [Arguments]    ${from}
    Input Text    id=bonusRangeFrom    ${from}
    
Input Bonus Range To
    [Arguments]    ${to}
    Input Text    id=bonusRangeTo    ${to}
    
Input Bonus Tier Percentage
    [Arguments]    ${per}
    Input Text    id=bonusPercentage    ${per} 
    
Bonus Tier Delete
    [Arguments]    ${num}
    Click Element    xpath=(//i[@class="fas fa-trash-alt"])    ${num}
                     
Edit Bonus Tier Name
    [Arguments]    ${num}
    Click Element    //i[@class="fas fa-pencil-alt"]    ${num}
    
Click On Distributor
    Click Element    //a[@href="/Distributor/Index"]

Click to Select Distributor Type For Distributor
    Click Element    id=distributorType
    
Select Distributor Type For Distributor
    [Arguments]    ${num}
    Select From List By Value    id=distributorType    ${num}
    
Input Distributor Description
    [Arguments]    ${description}
    Press Keys    id=disdescription    ${description}
    
Input Distributor Address
    [Arguments]    ${add}
    Input Text    id=disAddress    ${add}
    
Input Latitude
    [Arguments]    ${num}
    Input Text    id=disLatitude    ${num} 
    
Input Distributor Phone Number
    [Arguments]    ${num}
    Input Text    id=disMobile    ${num}    
    
Input Distributor Website
    [Arguments]    ${web}
    Input Text    id=disWebsite    ${web} 
    
Input Credit Limit
    [Arguments]    ${limit}
    Input Text    id=creditLimitAmount    ${limit}
    
Select Picture
    Choose File    id=disImage    C:/Users/Brainnovation/Pictures/Saved Pictures/a.jpg    
    
Input Distributor Email
    [Arguments]    ${email}
    Input Text    id=disEmail    ${email} 
    
Input Distributor Name
    [Arguments]    ${name}
    Input Text    id=distributorName    ${name}  
    
Click Submit Button
    Click Element    //button[@class="btn btn-primary"]    
    
Distributor Type Error Message Should be
    [Arguments]    ${message}
    Element Text Should Be    id=distributorType-error    ${message}    
    
Distributor Mobile No Error Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=disMobile-error    ${message}    
    
Credit Limit Error Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=creditLimitAmount-error    ${message}    
    
Distributor Name Error Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=distributorName-error    ${message}
    
Distributor Email Error Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=disEmail-error    ${message}    
    
Commission Rule Error Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=CommissionRuleName-error    ${message}    
    
Commission Rule Percentage Error Message Should Ne
    [Arguments]    ${message}
    Element Text Should Be    id=percentage-error    ${message}
    
Bonus Tier Name Error Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=bonusTierName-error    ${message}    
    
Bonus Range From Error Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=bonusRangeFrom-error    ${message}    
    
Bonus Range To Error Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=bonusRangeTo-error    ${message}
    
Bonus Percentage Error Message Should Be
    [Arguments]     ${message}
    Element Text Should Be    id=bonusPercentage-error    ${message}    
    
Success Message Should Be  
    [Arguments]    ${message}
    Element Text Should Be    id=messageSpan    ${message}
    
Edit Distributor's Name
    #[Arguments]    ${num}
    Click Element    //i[@class="fas fa-pencil-alt"]    
    
Click on Credit Request
    Click Element    //a[@href="/Distributor/DistributorCredit/Index"]

Update Credit Limit
    Click Element    //i[@class="far fa-share-square"] 

Select Grade
    [Arguments]    ${num}
    Select From List By Value    id=gradeInModal    ${num}  
    
Select Subject
    [Arguments]    ${num}
    Select From List By Value    id=subjectInModal    ${num}  

Select Chapter Area
    [Arguments]    ${num}
    Select From List By Value    id=chapterAreaInModal    ${num} 
    
Click on Distributor Type
     Click Element    distributorTypeMenu
     
Input Distribotor Type Name
    [Arguments]    ${name}
    Input Text     id=disTypeName   ${name}  

    
    
   
    
      
    
    
    
    
           
    
    
    
    