*** Settings ***
Library    SeleniumLibrary  
Library    ../ExternalKeywords/UserKey.py    

*** Variables ***
${URL}    https://staging.neemacademy.com
${Browser}    chrome
${valid username}    mahesh@yopmail.com
${valid password}    1234a
${delay}    1


*** Keywords ***
Start Browser and Maximize
    Open Browser    ${URL}    ${Browser}
    Maximize Browser Window
    Set Selenium Speed    ${delay}
    Landing Page Should Be Open
    Go to Login Page

    
Landing Page Should Be Open
    Title Should Be    Neemacademy | Digitizing Learning Experience    

    
Go to Login Page
    Click Link        id = signInLink

Login With Credentials
    [Arguments]    ${username}    ${password}
    Input Username    ${username}
    Input UserPassword    ${password}
    Submit Credential
# Create Folder at RunTime
    # [Arguments]    ${foldername}    ${subfoldername}
    # Create Folder    ${foldername}   
    # Create Sub Folder    ${subfoldername}    
    
# Concatinate Username and Password
    # [Arguments]    ${username}    ${password}
    # ${resultval}=    Concatinate Two Value    ${username}    ${password}
    # Log    ${resultval}
 
Input Username
    [Arguments]    ${username}
    Input Text    id = UserEmail    ${username}

    
Input UserPassword
    [Arguments]    ${password}
    Input Password    id = UserPassword    ${password}

  
Submit Credential
    Click Button    id = Login

Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=spanPassword    ${message}   
Close Browser Window
    Close Browser
    
