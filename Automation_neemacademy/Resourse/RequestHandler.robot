*** Settings ***
Library    SeleniumLibrary
Library    Collections    
Library    RequestsLibrary
Library    String 
Library    JSONLibrary    
Library    SeleniumLibrary    
       

Resource    ../Resourse/Resourse.robot
Resource    ../Resourse/Resource1.robot
# Test Setup   Start Browser and Maximize

*** Variables ***
${baseurl}    http://stagingapi.neemacademy.com

${Auth}    Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBUElVc2VyIiwianRpIjoiNDUzOTNlZDEtMjVjOC00ZThkLTg2MWYtZjE2ZDFkNWNkNjhkIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIkFQSVVzZXIiLCJBZG1pbiJdLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiQVBJVXNlciIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvdXNlcmRhdGEiOiJ7XCJVc2VySURcIjoyLFwiVXNlck5hbWVcIjpcInVzZXJfaWRfMlwiLFwiSXNWYWxpZFRva2VuXCI6ZmFsc2UsXCJSb2xlc1wiOltdfSIsIm5iZiI6MTU3NDA1OTAyNiwiZXhwIjoxNTc0MDY2MjI2LCJpc3MiOiJOZWVtYWNhZGVteS5BUElUb2tlbklzc3VlciIsImF1ZCI6Imh0dHBzOi8vc3RhZ2luZ2FwaS5uZWVtYWNhZGVteS5jb20ifQ.Is1JrDJYvf-of_SAg-NIsJ03qw0slkqMxH-HtyaBIvg


${type}    application/json
${distributor_type}    Distributor_type

*** Keywords ***
GetRequestCommonFunction
    [Arguments]    ${sub_url}    
    Create Session    GetRequest    ${baseurl}
    ${header} =    Create Dictionary     Content-Type=${type}    Authorization=${Auth}
    ${Response} =  Get Request    GetRequest    ${sub_url}    headers=${header}
    [Return]    ${Response}

PostRequestCommonFunction
    [Arguments]    ${sub_url}    ${body}
    Create Session    PostRequest    ${baseurl}    
    ${header} =    Create Dictionary     Content-Type=${type}    Authorization=${Auth}
    ${Response} =  Post Request    PostRequest    ${sub_url}    data=${body}    headers=${header}
    [Return]    ${Response}

PutRequestCommonFunction
    [Arguments]    ${sub_url}    ${body}
    Create Session    PutRequest    ${baseurl}    
    ${header} =    Create Dictionary     Content-Type=${type}    Authorization=${Auth}
    ${Response} =  Put Request    PutRequest    ${sub_url}    data=${body}    headers=${header}
    [Return]    ${Response}
    
DeleteRequestCommonFUnction
    [Arguments]    ${sub_url}    
    Create Session    Delete Request    ${baseurl}  
    ${header} =    Create Dictionary    Content-Type=${type}    Authorization=${Auth}
    ${response} =    Delete Request    DeleteRequest    ${sub_url}    headers=${header}           
    [Return]    ${Response}
           



    

        
    
    
    

    
    

    

    

   
   
    
    
    




