*** Settings ***
Library    SeleniumLibrary  
Library    ../ExternalKeywords/UserKey.py    

*** Variables ***
${URL}    https://staging.neemacademy.com
${Browser}    chrome
${valid username}    mahesh@yopmail.com
${valid password}    1234a
${delay}    1

*** Keywords ***
Input Email
    [Arguments]    ${username}
    Input Text    id = UserEmail    ${username}

    
Input Password
    [Arguments]    ${password}
    SeleniumLibrary.Input Password    id = UserPassword    ${password}

  
Submit 
    Click Button    id = Login

Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=spanPassword    ${message}  

ForgotPassword Link Click
    Click Link    //a[@href="/ResetPassword"]
    
Input ForgotEmail
    [Arguments]    ${forgotemail}
    Input Text    id=forgetemail    ${forgotemail}
    
PasswordReset link Send
    Click Element    id=ForgetPassword   
    
Verified Forgot Password Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    //p[@id="resetSuccessMsg"]    ${message}  
    # Forgot Password
    # [Arguments]    ${forgotemail}
   # Input UserEmail   ${forgotemail}
   
Invalid Forgot Password Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=invalidForgetEmail    ${message}
    
Not Verified Forgot Mail Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=invalidForgetEmail    ${message}
    
Empty Forgot Mail Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=invalidForgetEmail    ${message}
    
Wrong Forgot Mail Message Should be
    [Arguments]    ${message}
    Element Text Should Be    id=invalidForgetEmail    ${message} 
    
Input Forgot Number
    [Arguments]    ${forgotnum}
    Input Text    id=forgetemail    ${forgotnum}  
    
Registered Number Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=resetSuccessMsg    ${message}
    
Input Distributor Type
    [Arguments]    ${name}
    Input Text    id=disTypeName    ${name}    

Input Percentage
    [Arguments]    ${per}
    Input Text    id=disPercentage    ${per}    
    
Login
    Input Email    admin@neemacademy.com
    Input Password    User@123
    Submit
    
Distributor Type
    Click Element    xpath=(//span)[21]
    Click Link       id=distributorMenu 
    Click Element    //ol[@class="breadcrumb"]
    Click Element    //button[@class="btn btn-outline-success mr-2"]   
    
Add Distributor Type page Open
   Login
   Distributor Type 
   
New Distributor
    Input Distributor Type    Haru
    Input Percentage    5
    
Click Save Button
    Click Button    id=Save   
    
Distributor's Name without Percentage Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=disPercentage-error    ${message}
    
Distributor's Name Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=disPercentage-error    ${message}   
    
Add distributor's without Name Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=disTypeName-error    ${message}
    
Add Distributor with Empty Fields Message Should Be
    [Arguments]    ${message}
    Element Text Should Be    id=    expected    


    

    
    
       
   
     
      
        
        
    
    
    
        
      
    

       
